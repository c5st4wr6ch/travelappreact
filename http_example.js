import React, { Component } from 'react'
import { View, Text } from 'react-native'
class HttpExample extends Component {
	state = {
		data: ''
	}
	componentDidMount = () => {
		fetch('https://f6cc86ea-1d23-4d33-949b-a7c6b0e0f7c2.mock.pstmn.io/get-tours', {
			method: 'GET'
		})
		.then((response) => response.json())
		.then((responseJson) => {
			console.log(responseJson);
			this.setState({
				data: responseJson
			})
		})
		.catch((error) => {
			console.error(error);
		});
	}
	render() {
		return (
			<View>
				<Text>
					{this.state.data.body}
				</Text>
			</View>
		)
	}
}
export default HttpExample