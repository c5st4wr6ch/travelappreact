import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import PresentationalComponent from './PresentationalComponent';

export default class App extends React.Component {
    state = {
        myState: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis ostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    }
    updateState = () => {
        this.setState({ myState: 'The state is updated' })
    }

    render() {
        return (
            // <View style={styles.container}>
            //     <Text>Open up App.js to start working on your app!blabla</Text>
            //     <Text>Changes you make will automatically reload.</Text>
            //     <Text>Shake your phone to open the developer menu.</Text>
            // </View>

            <View>
                <PresentationalComponent myState = {this.state.myState} updateState = {this.updateState}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
